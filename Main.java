import java.util.ArrayList;
import java.util.Comparator;
import java.util.function.Predicate;

/**
 * Created by Антон on 11.11.2015.
 */
public class Main {
    public static void main(String...args)
    {
        Comparator<Integer> comp = (o1, o2) -> o1.compareTo(o2);
        Predicate<Integer> predicate = t -> t > 1;
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(9);
        list.add(8);
        list.add(0);
        list.add(97);
        RealiseIterativeParallelism r = new RealiseIterativeParallelism();
        System.out.println("Minimum: "+r.minimum(3,list,comp));
        System.out.println("Maximum: "+r.maximum(3,list,comp));
        System.out.println("All: "+r.all(3,list,predicate));
        System.out.println("Any: "+r.any(3,list,predicate));
        System.out.println("Filter: "+r.filter(3,list,predicate));
        System.out.println("Map: "+r.map(3,list,Integer::toBinaryString));
    }
}
