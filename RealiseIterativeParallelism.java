import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import static java.util.stream.Collectors.toList;

/**
 * Created by Антон on 11.11.2015.
 */
public class RealiseIterativeParallelism implements IterativeParallelism {


    @Override
    public <T> T minimum(int threads, List<T> list, Comparator<T> comparator) {
        return init(threads,list,
                results -> results.stream().min(comparator).orElse(null),
                splits -> splits.stream().min(comparator).orElse(null));
    }

    @Override
    public <T> T maximum(int threads, List<T> list, Comparator<T> comparator) {
        return init(threads,list,
                results -> results.stream().max(comparator).orElse(null),
                splits -> splits.stream().max(comparator).orElse(null));
    }

    @Override
    public <T> boolean all(int threads, List<T> list, Predicate<T> predicate) {
        return init(threads,list,
                results -> results.stream().allMatch(predicate),
                splits -> splits.stream().allMatch(t -> t));
    }

    @Override
    public <T> boolean any(int threads, List<T> list, Predicate<T> predicate) {

        return init(threads,list,
                results -> results.stream().anyMatch(predicate),
                splits -> splits.stream().anyMatch(t -> t));
    }

    @Override
    public <T> List<T> filter(int threads, List<T> list, Predicate<T> predicate) {

        return init(threads,list,
                results -> results.stream().filter(predicate).collect(toList()),
                splits -> splits.stream().flatMap(Collection::stream).collect(toList()));
    }

    @Override
    public <T, R> List<R> map(int threads, List<T> list, Function<T, R> function) {
        return init(threads,list,
                results -> results.stream().map(function).collect(toList()),
                splits -> splits.stream().flatMap(Collection::stream).collect(toList()));
    }

    private static <T,U,Z> Z init(int threads,List<T> list,Function<List<T>, U> func,Function<List<U>, Z> split) {
        List<List<T>> subs = new ArrayList<>();
        int a = list.size()/threads;
        for(int i = 0;i<threads-1;i++)
        {
            subs.add(list.subList(0,a));
            list = list.subList(a,list.size());
        }
        subs.add(list.subList(0,list.size()));
        ArrayList<U> res = new ArrayList<>();
        for(int i = 0;i<threads;i++)
        {
            Work w = new Work(subs.get(i),func);
            Thread t = new Thread(w);
            t.start();
            try
            {
                t.join();
                res.add(i,(U)w.result);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }

        return split.apply(res);
    }

    static class Work<T,U> implements Runnable {
        Function<T,U> func;
        T list;
        U result;
        public Work(T arr, Function<T,U> function)
        {
            list = arr;
            func = function;
        }
        @Override
        public void run() {
            result = func.apply(list);
        }
    }
}
